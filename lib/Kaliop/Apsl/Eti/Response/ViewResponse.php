<?php

namespace Kaliop\Apsl\Eti\Response;

use Kaliop\Apsl\Eti\View\View;

class ViewResponse extends Response
{
    protected $view;

    public function __construct(View $view)
    {
        $this->view = $view;
    }

    public function send() {
        $this->headers();
        $this->view->render();
    }
}
