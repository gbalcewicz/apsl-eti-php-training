<?php

namespace Kaliop\Apsl\Eti\Response;

class RedirectResponse extends Response
{
    /**
     * @var string
     */
    protected $url;

    /**
     * RedirectResponse constructor.
     * @param string $url
     */
    public function __construct($url)
    {
        $this->url = $url;
    }

    public function headers()
    {
        parent::headers();
        header(sprintf('Location: %s', $this->url));
    }

    public function send()
    {
        parent::send();
        exit();
    }
}
