<?php

namespace Kaliop\Apsl\Eti\DB;


class Manager
{
    /**
     * @var Connection
     */
    protected $connection;

    protected $mappings = [];

    /**
     * Manager constructor.
     * @param Connection $connection
     */
    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    public function setMapping($class, MappingInterface $mapping)
    {
        $this->mappings[$class] = $mapping;
    }

    /**
     * @param $class
     * @return MappingInterface
     * @throws \Exception
     */
    public function getMapping($class)
    {
        if (!isset($this->mappings[$class])) {
            throw new \Exception(sprintf('Class %s is not a mapped entity', $class));
        }

        return $this->mappings[$class];
    }

    /**
     * @param EntityInterface $entity
     * @throws \Exception
     */
    public function save(EntityInterface $entity)
    {
        $mapping = $this->getMapping(get_class($entity));

        if (empty($entity->getId())) {
            $fields = $mapping->getFields();
            $columnNames = implode(',', array_keys($fields));
            $values = implode(',', $this->getEntityValues($entity));

            $sql = sprintf('INSERT INTO %s(%s) VALUES (%s)', $mapping->getTableName(), $columnNames, $values);
        } else {
            $columnValuePairs = [];
            foreach ($this->getEntityValues($entity) as $columnName => $value) {
                $columnValuePairs = sprinf('%s=%s', $columnName, $value);
            }

            $sql = sprintf('UPDATE %s SET %s', $mapping->getTableName(), implode(' ', $columnValuePairs));
        }

        // TODO: Refactor to set values by statement bindValue
        $this->connection->executeQuery($sql);
    }

    /**
     * @param EntityInterface $entity
     */
    public function delete(EntityInterface $entity)
    {
        $mapping = $this->getMapping(get_class($entity));

        if (empty($entity->getId())) {
            // INFO: Do nothing this entity is not saved in database
            return;
        }

        $this->connection->executeQuery(sprintf(
            'DELETE FROM %s WHERE %s=%s LIMIT 1',
            $mapping->getTableName(),
            $mapping->getIdColumnName(),
            $entity->getId()
        ));
    }
    
    /**
     * @param EntityInterface $entity
     * @return array
     * @throws \Exception
     */
    protected function getEntityValues(EntityInterface $entity)
    {
        $mapping = $this->getMapping(get_class($entity));
        $values = [];

        foreach ($mapping->getFields() as $columnName => $property) {
            $methodName = 'get' . ucfirst($property);

            if (!method_exists($entity, $methodName)) {
                throw new \Exception(sprintf('Getter for entity %s does not exist', $methodName));
            }

            $value = $entity->$methodName();
            // TODO: determine other value types
            $values[] = is_numeric($value) ? $value : sprintf("'%s'", $value);
        }

        return $values;
    }
}
