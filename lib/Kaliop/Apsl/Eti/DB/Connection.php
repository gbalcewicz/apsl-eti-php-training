<?php

namespace Kaliop\Apsl\Eti\DB;

class Connection
{
    /**
     * @var \PDO
     */
    private $pdo;

    /**
     * Connection constructor.
     *
     * @param string $database
     * @param string $user
     * @param string $password
     * @param string $host
     */
    public function __construct($database, $user, $password, $host = 'localhost')
    {
        $dsn = sprintf('mysql:host=%s;dbname=%s', $host, $database);
        $this->pdo = new \PDO($dsn, $user, $password, [
            \PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"
        ]);
    }

    /**
     * @param $sql
     * @throws \Exception
     */
    public function executeQuery($sql)
    {
        // INFO: wyrzucenie wyjątku gdy nie udało wykonać się zapytania do bazy danych
        if (!$this->pdo->exec($sql)) {
            $errorInfo = $this->pdo->errorInfo();

            throw new \Exception(sprintf('[%d] %s', $errorInfo[1], $errorInfo[2]));
        }
    }

    /**
     * @return \PDO
     */
    public function getPdo()
    {
        return $this->pdo;
    }
}
