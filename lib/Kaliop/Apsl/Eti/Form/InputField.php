<?php

namespace Kaliop\Apsl\Eti\Form;

class InputField extends AbstractField
{
    /**
     * @var string
     */
    protected $type;

    /**
     * InputField constructor
     *
     * @param $name
     * @param string $value
     * @param string $label
     * @param string $type
     */
    public function __construct($name, $value = '', $label = '', $type = 'text')
    {
        parent::__construct($name, $value, $label);
        $this->type = $type;
    }

    /**
     * @param string $formName
     * @return string
     */
    public function render($formName = '')
    {
        $str = sprintf('<label>%s</label>', $this->label);
        $str .= sprintf('<input name="%s" type="%s" value="%s" />', $this->generateName($formName), $this->type, $this->value);

        return $str;
    }
}
