<!DOCTYPE html>
<html>
    <head>
        <title>Strona główna</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
    </head>
    <body>
        <header>
            <h1>APSL-ETI Training</h1>
        </header>

        <nav>
            <ul>
                <li><a href="index.php?action=addTournament">Dodaj turniej</a></li>
            </ul>
        </nav>
        <?php echo $content ?>

    </body>
</html>
